#!/bin/bash

echo "Running as `whoami`"

declare -rix MIN_DEV_SIZE=$((2*1023*1048576)) # 1G minimum

. $MAINDIR/impl/common.sh


if [ "$DEVICE_SIZE" -lt "$MIN_DEV_SIZE" ]; then
  echo "Device size is too small $(($DEVICE_SIZE/(1024*1024))) MB" 1>&2
  echo "Required size is at least 1024MB" 1>&2
  exit 1
fi

#partition
cmd_exists "parted"
echo "Creating GPT partitions" && parted -a optimal -s $blockdev mklabel gpt mkpart grub 1MiB 10MiB set 1 bios_grub on mkpart main ext4 10MiB 100% set 2 boot on align-check optimal 1 align-check optimal 2 print quit

#settle down
do_sync
sleep 1
do_sync

map_disk

echo "Putting root filesystem into $ROOT_DEV"
cmd_exists "mkfs"
cmd_exists "mkfs.ext4"
echo "Formatting EXT4 on ${ROOT_DEV}" && mkfs -F -t ext4 ${ROOT_DEV}
root_uuid=$(${VOL_ID} ${ROOT_DEV} )

do_sync

cmd_exists "mktemp"
cmd_exists "rmdir"
echo "Creating mountpoint" && ROOT_DIR=$(mktemp -d) && FRAMEWORK_CLEANUP+=("rmdir ${ROOT_DIR}")

echo "Mounting root filesystem" && mount ${ROOT_DEV} ${ROOT_DIR} && FRAMEWORK_CLEANUP+=("umount ${ROOT_DIR}")

cmd_exists "cdebootstrap-static"
echo "Using ${ROOT_DIR} to debootstrap the system"
cdebootstrap-static --arch=${DPKG_ARCH} --include=bash,bash-completion,bash-builtins,linux-image-amd64,acpid,sudo,grub-pc,ansible,git,vim,mc,ssh,python-apt stretch ${ROOT_DIR} http://127.0.0.1:3142/ftp.sk.debian.org/debian/

mount_virtual_filesystems ${ROOT_DIR}

chroot $ROOT_DIR passwd -d root
chroot $ROOT_DIR adduser --gecos linuxadmin --disabled-password linuxadmin
chroot $ROOT_DIR adduser linuxadmin sudo
chroot $ROOT_DIR usermod -p ..4/MD05HLWjI linuxadmin #password is 'a'

rm -rf $ROOT_DIR/etc/udev/rules.d/70-persistent-cd.rules
rm -rf $ROOT_DIR/etc/udev/rules.d/70-persistent-net.rules

chroot $ROOT_DIR apt-get clean

# reset the root password
chroot $ROOT_DIR passwd -d root

#fixme this is wrong
cp -p /etc/hosts $ROOT_DIR/etc/hosts
cp -p /etc/resolv.conf $ROOT_DIR/etc/resolv.conf
echo $instance > $ROOT_DIR/etc/hostname
echo $instance > $ROOT_DIR/etc/mailname

cat > $ROOT_DIR/etc/fstab <<EOF
# /etc/fstab: static file system information.
#
# <file system>   <mount point>   <type>  <options>       <dump>  <pass>
UUID=$root_uuid   /               ext4    defaults        0       1
proc              /proc           proc    defaults        0       0
EOF

cat > $ROOT_DIR/etc/network/interfaces <<EOF
auto lo
iface lo inet loopback
EOF

chroot $ROOT_DIR usr/sbin/update-initramfs -u

install_grub ${ROOT_DIR} ${blockdev}

#basic install is ready now

#lets customize
cp -rv $MAINDIR/transfer/* $ROOT_DIR/root/

# run customizations
chroot $ROOT_DIR /bin/bash -c "./root/customize.sh"

currdir=$(pwd)
tmpdir=$(mktemp -d)
cd ${tmpdir}
git clone https://bitbucket.org/jurajholtak-devops/linux-setup.git
cd linux-setup/ansible
cat > chroots <<EOF
[localhost]
${ROOT_DIR}
EOF
sudo ansible-playbook -v -c chroot -i 'chroots' --extra-vars "restart_services=no" debian-ganeti-vm.yml
cd ${currdir}

#sync to be on the safe side
do_sync

# execute cleanups
cleanup
trap - EXIT

exit 0





