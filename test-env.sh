#!/bin/bash

#config
IMAGE_SIZE_MBYTE=2048

echo "Bootstrap test environment...."

#MAINDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
MAINDIR=/tmp/ramdisk
echo "MAINDIR=$MAINDIR"
BUILDDIR=$MAINDIR/build
echo "BUILDDIR=$BUILDDIR"

echo "Creating build directory if not present" && mkdir -p $BUILDDIR

IMAGE=$BUILDDIR/image3
echo "Will use image $IMAGE"

if [ ! -f $IMAGE ]; then
    echo "Creating image $IMAGE" && dd if=/dev/zero of=$IMAGE bs=1M count=$IMAGE_SIZE_MBYTE
fi

echo "Exported config:"
export OS_API_VERSION=20 && echo "OS_API_VERSION=$OS_API_VERSION"
export HYPERVISOR=kvm && echo "HYPERVISOR=$HYPERVISOR"
export DISK_COUNT=1 && echo "DISK_COUNT=$DISK_COUNT"
export DISK_0_PATH=$IMAGE && echo "DISK_0_PATH=$DISK_0_PATH"
export INSTANCE_NAME=test_instance && echo "INSTANCE_NAME=$INSTANCE_NAME"



