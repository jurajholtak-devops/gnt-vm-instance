#!/bin/bash

declare -a FRAMEWORK_CLEANUP
declare MAIN_FILESYSTEM

echo `env`

log_error() {
  echo "$@" >&2
}

bailout() {
    log_error $1
    exit 1
}

do_sync() {
    echo "Sync drives..."
    sync
}

cmd_present() {
    type "$1" &> /dev/null ;
}

cmd_exists() {
    if ! cmd_present $1 ; then
        bailout  "Command $1 is not present. Exiting..."
    fi
}

echo_done() {
    echo "done"
}

cleanup() {
  echo "Executing cleanup..."
  print_cleanup
  if [ ${#FRAMEWORK_CLEANUP[*]} -gt 0 ]; then
    LAST_ELEMENT=$((${#FRAMEWORK_CLEANUP[*]}-1))
    REVERSE_INDEXES=$(seq ${LAST_ELEMENT} -1 0)
    for i in ${REVERSE_INDEXES}; do
      echo "${FRAMEWORK_CLEANUP[$i]}"
      ${FRAMEWORK_CLEANUP[$i]}
    done
  fi
}

blockdevice_check() {
     cmd_exists "losetup"
     cmd_exists "udevadm"
    # If the target device is not a real block device we'll first losetup it.
    # This is needed for file disks.
    if [ ! -b ${blockdev} ]; then
      echo "Not working on block device, using losetup..."
      ORIGINAL_BLOCKDEV=${blockdev}
      blockdev=$(losetup --show -f ${blockdev})
      FRAMEWORK_CLEANUP+=("losetup -d ${blockdev}")
      FRAMEWORK_CLEANUP+=("udevadm settle")
    fi

    if [ ! -b "${blockdev}" ];
    then
        echo "Target is not a block device! Can\`t proceed with the installation..."
        exit 1
    fi
}

map_disk() {
    cmd_exists "kpartx"
    local filesystem_dev_base=`kpartx -l -pp ${blockdev} | \
                       grep -m 1 -- "p2 : .*${blockdev}" | \
                       awk '{print $1}'`
    if [ -z "$filesystem_dev_base" ]; then
        bailout "Cannot interpret kpartx output and get partition mapping"
    fi

    kpartx -a -s -pp ${blockdev} > /dev/null && FRAMEWORK_CLEANUP+=("unmap_disk")

    local filesystem_dev="/dev/mapper/$filesystem_dev_base"
    if [ ! -b "$filesystem_dev" ]; then
        bailout "Can't find kpartx mapped partition: ${filesystem_dev}"
    fi
    echo "Mapped main filesystem as ${filesystem_dev}"
    ROOT_DEV=${filesystem_dev}
}

unmap_disk() {
    cmd_exists "kpartx"
    do_sync
    do_sync
    do_sync
    sleep 1
    echo "kpartx -d -pp ${blockdev}" && kpartx -d -pp ${blockdev}
}

print_cleanup() {
    echo "Cleanup Structure:"
    printf 'CLEANUP->%s\n' "${FRAMEWORK_CLEANUP[@]}"
    echo
}

mount_virtual_filesystems() {
    echo "Mounting virtual filesystems ( /dev /dev/pts /proc /sys )"
    mount /dev -t devfs -obind $1/dev
    mount /dev/pts -t devpts -obind $1/dev/pts
    mount /proc -t proc -obind $1/proc
    mount /sys -t sysfs -obind $1/sys
    FRAMEWORK_CLEANUP+=("unmount_virtual_filesystems $1")
}

unmount_virtual_filesystems() {
    echo "UnMounting virtual filesystems ( /dev /dev/pts /proc /sys )"
    umount $1/sys
    umount $1/proc
    umount $1/dev/pts
    umount $1/dev
}

install_grub() {
    echo "Installing grub into $2"
    chroot $1 update-grub
    umount $1/proc
    chroot $1 grub-install --no-floppy --modules="part_msdos part_gpt lvm ext2" $2
    mount /proc -t proc -obind $1/proc
    chroot $1 update-grub
}

get_api5_arguments() {
  GETOPT_RESULT=$*
  # Note the quotes around `$TEMP': they are essential!
  eval set -- "$GETOPT_RESULT"
  while true; do
    case "$1" in
      -i|-n) instance=$2; shift 2;;

      -o) old_name=$2; shift 2;;

      -b) blockdev=$2; shift 2;;

      -s) swapdev=$2; shift 2;;

      --) shift; break;;

      *)  log_error "Internal error!" >&2; exit 1;;
    esac
  done
  if [ -z "$instance" -o -z "$blockdev" ]; then
    log_error "Missing OS API Argument (-i, -n, or -b)"
    exit 1
  fi
  if [ "$SCRIPT_NAME" != "export" -a -z "$swapdev"  ]; then
    log_error "Missing OS API Argument -s (swapdev)"
    exit 1
  fi
  if [ "$SCRIPT_NAME" = "rename" -a -z "$old_name"  ]; then
    log_error "Missing OS API Argument -o (old_name)"
    exit 1
  fi
}

get_api10_arguments() {
  if [ -z "$INSTANCE_NAME" -o -z "$HYPERVISOR" -o -z "$DISK_COUNT" ]; then
    log_error "Missing OS API Variable:"
    log_error "(INSTANCE_NAME HYPERVISOR or DISK_COUNT)"
    exit 1
  fi
  instance=$INSTANCE_NAME
  if [ $DISK_COUNT -lt 1 -o -z "$DISK_0_PATH" ]; then
    log_error "At least one disk is needed"
    exit 1
  fi
  if [ "$SCRIPT_NAME" = "export" ]; then
    if [ -z "$EXPORT_DEVICE" ]; then
      log_error "Missing OS API Variable EXPORT_DEVICE"
    fi
    blockdev=$EXPORT_DEVICE
  elif [ "$SCRIPT_NAME" = "import" ]; then
    if [ -z "$IMPORT_DEVICE" ]; then
       log_error "Missing OS API Variable IMPORT_DEVICE"
    fi
    blockdev=$IMPORT_DEVICE
  else
    blockdev=$DISK_0_PATH
  fi
  if [ "$SCRIPT_NAME" = "rename" -a -z "$OLD_INSTANCE_NAME" ]; then
    log_error "Missing OS API Variable OLD_INSTANCE_NAME"
  fi
  old_name=$OLD_INSTANCE_NAME
}


trap cleanup EXIT


cmd_exists "blkid"


if [ -f /sbin/blkid -a -x /sbin/blkid ]; then
  VOL_ID="blkid -o value -s UUID"
  VOL_TYPE="blkid -o value -s TYPE"
else
  for dir in /lib/udev /sbin; do
    if [ -f $dir/vol_id -a -x $dir/vol_id ]; then
      VOL_ID="$dir/vol_id -u"
      VOL_TYPE="$dir/vol_id -t"
    fi
  done
fi

if [ -z "$VOL_ID" ]; then
  log_error "vol_id or blkid not found, please install udev or util-linux"
  exit 1
fi

if [ -z "$OS_API_VERSION" -o "$OS_API_VERSION" = "5" ]; then
  OS_API_VERSION=5
  GETOPT_RESULT=`getopt -o o:n:i:b:s: -n '$0' -- "$@"`
  if [ $? != 0 ] ; then log_error "Terminating..."; exit 1 ; fi
  get_api5_arguments $GETOPT_RESULT
elif [ "$OS_API_VERSION" = "10" -o "$OS_API_VERSION" = "15" -o \
       "$OS_API_VERSION" = "20" ]; then
  get_api10_arguments
else
  log_error "Unknown OS API VERSION $OS_API_VERSION"
  exit 1
fi

VARIANTS_DIR=config/variants

if [ -n "$OS_VARIANT" ]; then
  if [ ! -d "$VARIANTS_DIR" ]; then
    log_error "OS Variants directory $VARIANTS_DIR doesn't exist"
    exit 1
  fi
  VARIANT_CONFIG="$VARIANTS_DIR/$OS_VARIANT.conf"
  if [ -f "$VARIANT_CONFIG" ]; then
    . "$VARIANT_CONFIG"
  else
    if grep -qxF "$OS_VARIANT" variants.list; then
      log_error "ERROR: instance-debootstrap configuration error"
      log_error "  Published variant $OS_VARIANT is missing its config file"
      log_error "  Please create $VARIANT_CONFIG or unpublish the variant"
      log_error "  (by removing $OS_VARIANT from variants.list)"
    else
      log_error "Unofficial variant $OS_VARIANT is unsupported"
      log_error "Most probably this is a user error, forcing a wrong name"
      log_error "To support this variant please create file $VARIANT_CONFIG"
    fi
    exit 1
  fi
fi


echo "Processing with ${blockdev}"

echo "Framework v1"
blockdevice_check

echo "Using ${blockdev}"

DEVICE_SIZE=$(blockdev --getsize64 ${blockdev})
echo "Device size is $(($DEVICE_SIZE/(1024*1024))) MB"
